//: Playground - noun: a place where people can play

import UIKit

/**
 *   0. 可选类型
 */
var someApple: Int?
someApple = nil

func optionalDemo() {
    let arrInt:[Int?] = [10,nil,20]
    for i in 0...2 {
        if arrInt[i] != nil {
            var a = arrInt[i]
            a = a! + 2
            print("强制解包\(a!)")
        }
        
        if let a  = arrInt[i] {
            print("可选绑定\(a)")
        }
    }
}

//optionalDemo()

/**
 *   1.闭包
 */

func square(a:Float) -> Float {
    return a*a
}

func cube(a:Float) -> Float {
    return a*a*a
}

func average(a:Float, b:Float, r:((Float) -> Float)) -> Float {
    return (r(a) + r(b)) / 2;
}

average(a: 4, b: 2, r: square)
average(a: 4, b: 2, r: cube)


/**
 * 2. map
 */
func mapDemo() {
    
    let arrInt = [10,20,33, 54]
    
    // 传统方法
    var arrAdd = [Int]()
    for var number in arrInt {
        number += 2
        arrAdd.append(number)
    }
    print(arrAdd)
    
    // map
    arrInt.map { (a:Int) -> Int in
        return a + 2
    }
}

//mapDemo()

func flatMapDemo() {
    
    let fruits = ["apple", "banana", "orange", ""]
    
    let countMap = fruits.map { fruit -> Int? in
        let length = fruit.characters.count
        guard length > 0 else {
            return nil
        }
        return length
    }
    countMap
    
//   1. flatMap返回后的数组中不存在nil 同时它会把Optional解包
    let countFlatMap = fruits.flatMap { fruit -> Int? in
        let length = fruit.characters.count
        guard length > 0 else {
            return nil
        }
        return length
    }
    countFlatMap
    
//   2. flatMap 还能把存有数组的数组 合并为新的一个数组
    let array = [[1,2,3], [4,5,6], [7,8,9]]
    let arrayMap = array.map { $0 }
    arrayMap

    let arrayFlatMap = array.flatMap { $0 }
    arrayFlatMap
    
    array.flatMap { (a: [Int]) -> [Int] in
        return a
    }
    
    array.flatMap { (a:[Int]) -> [Int]? in
        return a
    }
    
    
//   3. flatMap也能把两个不同的数组合并成一个数组 这个合并的数组元素个数是前面两个数组元素个数的乘积
    let apples = ["apple"]
    let counts = [1, 2, 3] 
    let appleCounts = counts.flatMap { count in
        apples.flatMap { apple in
            (apple, count)
        }
    }
    print(appleCounts)
}

//flatMapDemo()

